

export namespace Helpers {
    export function arrayPropertySort<E>(array: E[], property: string, descending: boolean): E[] {
        const sorted = array.sort((a, b) => {
            if(a[property] < b[property]) return -1;
            if(a[property] > b[property]) return 1;
            return 0;
        });

        if(descending) return sorted.reverse();
        return sorted;
    }
}