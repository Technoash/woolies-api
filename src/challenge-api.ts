import axios from "axios";
import { Service } from "typedi";
import { ConfigService } from "./config";



export type IShopperHistoryResponse = ICustomer[];
export type IProductsResponse = IProduct[];

export interface ICustomer {
    customerId: string,
    products: IProduct[] 
}

export interface IProduct {
    name: string,
    price: number,
    quantity: number
}


@Service()
export class ChallengeApiService {

    constructor(
        private readonly configService: ConfigService
    ){}

    async getShopperHistory() {
        const url = this.getResourceUrl('/resource/shopperHistory');
        const response = await axios.get<IShopperHistoryResponse>(url);
        return response.data;
    }

    async getProducts(){
        const url = this.getResourceUrl('/resource/products');
        const response = await axios.get<IProductsResponse>(url);
        return response.data;
    }

    private getResourceUrl(resource: string): string {
        return `${this.configService.getApiUrl()}${resource}?token=${this.configService.getApiToken()}`;
    }

}