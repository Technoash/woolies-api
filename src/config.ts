import { Service } from "typedi";
import * as config from "yaml-config";
import { join } from "path";
import { strict as assert } from 'assert';


const configFile = <IConfig> config.readConfig(join(__dirname, '../config/config.yaml'));

interface IConfig {
    API: {
        user_name: string;
        api_token: string;
        url: string;
    }
}

@Service()
export class ConfigService {


    getApiToken(){
        const token = configFile?.API?.api_token
        assert(token); // throw an error if the config item is not set
        return token;
    }

    getApiAuthorName(){
        const user_name = configFile?.API?.user_name;
        assert(user_name);
        return user_name;
    }

    getApiUrl(){
        const url = configFile?.API?.url;
        assert(url);
        return url;
    }

}