import { Service } from "typedi";
import { Helpers } from "./helpers";

interface ITrolleyProduct {
    Name: string,
    Price: number,
    Quantity?: number
}

interface IProductQuantity {
    Name: string;
    Quantity: number;
}

interface ISpecial {
    Quantities: IProductQuantity[];
    Total: number;
}

export interface ITrolleyCalculatorRequest {
    Products: ITrolleyProduct[];
    Specials: ISpecial[];
    Quantities: IProductQuantity[];
}

type ISpecialWithDiscount = ISpecial & { discount: number };

@Service()
export class TrolleyCalculatorService {

    calculateTrolleyTotal(trolleyRequest: ITrolleyCalculatorRequest): number {
        // find the discount amount for each special (specials will be applied from highest discount to lowest)
        const specials = new Set<ISpecialWithDiscount>();

        for (const special of trolleyRequest.Specials) {
            // sum up product totals (this is what the items would cost without a discount)
            const beforeSpecialTotal = special.Quantities.reduce((acc, product) => acc + product.Quantity * this.getPrice(product, trolleyRequest), 0);

            // the discount is the sum of product totals minus the special price
            specials.add({
                ...special,
                discount: beforeSpecialTotal - special.Total
            });
        }

        // apply specials from highest discount to lowest
        let total = 0;
        const quantities: Array<IProductQuantity> = JSON.parse(JSON.stringify(trolleyRequest.Quantities)); // clone quantities to avoid mutations of the request
        // sort the specials in order of hightest to lowest discount
        const sortedSpecials = Helpers.arrayPropertySort(Array.from(specials), 'discount', true);

        // go through special items in order and check if they apply
        for (const special of sortedSpecials) {
            if (this.isSpecialApplicable(special, quantities)) {
                // remove special items from the order 
                for (const item of special.Quantities) {
                    this.removeItemFromOrder(item.Name, item.Quantity, quantities);
                }
                // tally special totals
                total += special.Total;
            }
        }

        // add the price of any remaining (non-special products)
        for (const item of quantities) {
            total += this.getPrice(item, trolleyRequest) * item.Quantity;
        }

        return total;
    }


    /**
     * 
     * @param productq The product to get the price for
     * @returns Product price from the trolley request
     */
    private getPrice(productq: IProductQuantity, trolleyRequest: ITrolleyCalculatorRequest) {
        const product = trolleyRequest.Products.find(p => p.Name === productq.Name);
        if (!product) throw new Error(`Could not find product '${productq.Name}'`);
        return product.Price;
    }

    /**
     * 
     * @param special the special item and quantity to check
     * @param quantities the order to check against
     * @returns whether or not the special can be applied to the order
     */
    private isSpecialApplicable(special: ISpecialWithDiscount, order: Array<IProductQuantity>): boolean {
        // check if the order contains all of the special items (with minimum special quantity)
        for (const item of special.Quantities) {
            if (!order.find(p => p.Name === item.Name && p.Quantity >= item.Quantity)) return false;
        }
        return true;
    }

    /**
     * 
     * @param product The product to subtract from the order
     * @param quantity The amount to remove
     * @param order The order to remove from
     */
    private removeItemFromOrder(product_name: string, removeQuantity: number, order: Array<IProductQuantity>): void {
        if (removeQuantity < 1) return;
        const foundIndex = order.findIndex(p => p.Name === product_name);
        if (foundIndex < 0) throw new Error('Order does not have item');
        const found = order[foundIndex];
        if (found.Quantity < removeQuantity) throw new Error('Item does not have enough quantity to remove');
        found.Quantity -= removeQuantity;
        // if the item has zero quantity, remove it
        if (found.Quantity < 1) order.splice(foundIndex, 1);
    }
}