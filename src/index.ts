import 'reflect-metadata';
import 'source-map-support/register';
import express from 'express';
import * as bodyParser from 'body-parser';
import Container from 'typedi';
import { ConfigService } from './config';
import { ChallengeApiService, IProduct, IProductsResponse } from './challenge-api';
import { ESortOrder, ProductSortService, SORT_ORDERS } from './product-sort';
import { Helpers } from './helpers';
import { ITrolleyCalculatorRequest, TrolleyCalculatorService } from './trolley-calculator';

export const app = express();
app.use(bodyParser.json());
const configService = Container.get(ConfigService);
const challengeApiService = Container.get(ChallengeApiService);
const productSortService = Container.get(ProductSortService);
const trolleyCalculatorService = Container.get(TrolleyCalculatorService);


// Exercise 1
app.get('/user', async (req, res) => {
    res.json({
        name: configService.getApiAuthorName(),
        token: configService.getApiToken()
    });
});


// Exercise 2
app.get('/sort', async (req, res) => {

    const sort_order = <ESortOrder>req.query['sortOption'];
    if (!SORT_ORDERS.has(sort_order)) {
        return res.status(400).send(`Invalid sort order. Expecting ${Array.from(SORT_ORDERS).join(', ')}.`);
    }

    try {
        const products = await challengeApiService.getProducts();
        const sorted = await productSortService.sortProducts(products, sort_order);
        res.json(sorted);
    } catch (e) {
        res.status(500).send('Internal Error');
        throw e;
    }
});


// Exercise 3
app.post('/trolleyTotal', async (req, res) => {
    try {
        const trolleyRequest = <ITrolleyCalculatorRequest> req.body;
        res.send(trolleyCalculatorService.calculateTrolleyTotal(trolleyRequest).toString());
    } catch(e) {
        console.log('failed', `'${JSON.stringify(req.body)}'`);
        throw e;
    }
});