import { Service } from "typedi";
import { ChallengeApiService, IProduct } from "./challenge-api";
import { Helpers } from "./helpers";

export type ESortOrder = 'Low' | 'High' | 'Ascending' | 'Descending' | 'Recommended'
export const SORT_ORDERS: Set<ESortOrder> = new Set(['Low', 'High', 'Ascending', 'Descending', 'Recommended']);


@Service()
export class ProductSortService {

    constructor(
        private readonly challengeApiService: ChallengeApiService
    ){}

    async sortProducts(products: IProduct[], order: ESortOrder): Promise<IProduct[]> {


        switch(order){
            case 'Low':
                return Helpers.arrayPropertySort(products, 'price', false);
            case 'High':
                return Helpers.arrayPropertySort(products, 'price', true);
            case 'Ascending':
                return Helpers.arrayPropertySort(products, 'name', false);
            case 'Descending':
                return Helpers.arrayPropertySort(products, 'name', true);
            case 'Recommended':
                return await this.sortProductsByRecommended(products);
        }

        throw new Error(`Missing sorter for sort order '${order}'`);
    }

    private async sortProductsByRecommended(products: IProduct[]){
        const orders = await this.challengeApiService.getShopperHistory();

        // build a map of product name => total sales volume from order history
        const sales: {[product_name: string]: number} = {};
        for(const order of orders){
            for(const product of order.products){
                if(!(product.name in sales)) sales[product.name] = 0;
                sales[product.name] += product.quantity;
            }
        }

        function getSalesVolume(product: IProduct): number {
            return sales[product.name] || 0;
        }

        // use the product sales volume to sort the products array
        const sorted = products.sort((a, b) => {
            if(getSalesVolume(a) < getSalesVolume(b)) return -1;
            if(getSalesVolume(a) > getSalesVolume(b)) return 1;
            return 0;
        });

        return sorted.reverse();
    }

}